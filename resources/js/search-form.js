(function(){
    
    var searchButton = document.getElementById("search-button");

    searchButton.addEventListener("click", function(event) {
        
        button = event.target;

        form = button.closest("form");

        if(form.classList.contains("_active")) { 
            return true;
        }else{
            event.preventDefault(); 
            form.classList.add('_active');
            return false;
        }

    });

})();